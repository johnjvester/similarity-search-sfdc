# `similarity-search-sfdc`

> A [Spring Boot](<https://spring.io/projects/spring-boot>) RESTful API that is intended to run on 
> [Heroku](https://www.heroku.com/) and access a [Heroku Postgres](https://elements.heroku.com/addons/heroku-postgresql)
> database which is integrated with [Salesforce](https://www.salesforce.com/) `Account` objects 
> using [Heroku Connect](https://elements.heroku.com/addons/herokuconnect). The PostgreSQL instance 
> includes the [`pgvector` extension](https://github.com/pgvector/pgvector/) to perform similarity 
> searches for Salesforce accounts by industry.

## Publications

This repository is related to the following DZone.com publications:

* [Vector Tutorial: Conducting Similarity Search in Enterprise Data](https://dzone.com/articles/using-pgvector-to-locate-similarities-in-enterpris)

To read more of my publications or browse my public projects, please review one of the following URLs:

* https://dzone.com/authors/johnjvester
* https://gitlab.com/users/johnjvester/projects

## About this Service

The use case for the `similarity-search-sfdc` repository can be summarized as noted below:

* `Account` objects exist in Salesforce, with the `Industry` attribute populated.
* There is a desire to programmatically determine the closest matches for a given industry.
* Example: Which accounts are in industries that are similar to the Software industry?

The following illustration is a high-level design of the solution to meet the use case using Heroku:

![High-Level Implementation](images/similarity-search-sfdc.png)

The `similarity-search-sfdc` repository provides a RESTful API which serves as an entry point to solve 
the use case noted above.

## Using this Service

The following dependencies exist when using this service:

* PostgreSQL database with `pgvector` extension installed - which can be ran locally via [Docker Compose](docker-compose.yml) via the `docker-compose up -d database` command
* [`salesforce` schema](salesforce.databases.ddl) applied
* `salesforce.accounts` data (from Salesforce via Heroku Connect) ([sample data](salesforce.accounts.sql))
* `salesforce.industries` data (from [`generate-embeddings`](#about-generate-embeddings) repository) ([sample data](salesforce.industries.sql))

### About `generate-embeddings`

To generate word vectors (embeddings) on your own, please review the following (very simple) repository:

https://gitlab.com/johnjvester/generate-embeddings

Use of this Python-based repository will foster the creation of word vectors to match your own use case.

### GET Accounts

Use the following URI to obtain a list of accounts:

```shell
curl --location 'https://hostname/accounts'
```

This will return a `200 OK` response with the following payload:

```json
[
  {
    "id": "001Kd00001bsP80IAE",
    "name": "CleanSlate Technology Group",
    "industry": "Technology"
  },
  {
    "id": "001Kd00001bsP85IAE",
    "name": "Marqeta",
    "industry": "Technology"
  },

  ...
  
  {
    "id": "0011a00000gy7QhAAI",
    "name": "Dale Burgers",
    "industry": "Food & Beverage"
  }
]
```

### GET Similarities

To locate similarities for a known Salesforce industry, use the following URI:

```shell
curl --location 'https://hostname/accounts/similarities?industry=Software&limit=3'
```

This will return a `200 OK` response and the `3` (as specified by the `limit` 
request parameter) closest industries to the `Software` industry:

```json
[
    {
        "id": "001Kd00001bsP80IAE",
        "name": "CleanSlate Technology Group",
        "industry": "Technology"
    },
    {
        "id": "001Kd00001bsPBFIA2",
        "name": "CMG Worldwide",
        "industry": "Media"
    },
    {
        "id": "001Kd00001bsP8AIAU",
        "name": "Dev Spotlight",
        "industry": "Technology"
    },
    {
        "id": "001Kd00001bsP8hIAE",
        "name": "Egghead",
        "industry": "Electronics"
    },
    {
        "id": "001Kd00001bsP85IAE",
        "name": "Marqeta",
        "industry": "Technology"
    }
]
```

## Deploying Changes to Heroku

It is important that the `heroku` CLI is installed and the `heroku login` command has been executed.

Once the `main` branch is ready for deployment, simply execute the following command:

```shell
git push heroku main
```

If there is desire to push a branch other than `main` (lets say it is called `feature`), use the following command:

```shell
git push heroku feature:main
```

## Additional Information

Made with <span style="color:red;">♥</span> &nbsp;by johnjvester@gmail.com, because I enjoy writing code.
