package com.gitlab.johnjvester.similarity;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SimilaritySearchSfdcApplication {

    public static void main(String[] args) {
        SpringApplication.run(SimilaritySearchSfdcApplication.class, args);
    }

}
