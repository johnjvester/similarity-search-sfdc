package com.gitlab.johnjvester.similarity.services;

import com.gitlab.johnjvester.similarity.entities.Account;
import com.gitlab.johnjvester.similarity.entities.Industry;
import com.gitlab.johnjvester.similarity.repositories.AccountsRepository;
import com.gitlab.johnjvester.similarity.repositories.IndustriesRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Set;

@RequiredArgsConstructor
@Service
public class AccountsService {
    private final AccountsRepository accountsRepository;
    private final IndustriesRepository industriesRepository;

    public List<Account> getAccounts() {
        return accountsRepository.findAll();
    }

    public Set<Account> getAccountsBySimilarIndustry(String industry, int limit) throws Exception {
        List<Industry> industries = industriesRepository.findAll();

        if (industries.stream().map(Industry::getName).anyMatch(industry::equals)) {
            return accountsRepository.findSimilaritiesForIndustry(industry, limit);
        } else {
            throw new Exception("Could not locate '" + industry + "' industry");
        }
    }
}
