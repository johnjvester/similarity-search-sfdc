package com.gitlab.johnjvester.similarity.repositories;

import com.gitlab.johnjvester.similarity.entities.Account;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.Set;

public interface AccountsRepository extends JpaRepository<Account, String> {
    @Query(nativeQuery = true, value = "SELECT sfid, name, industry " +
            "FROM salesforce.account " +
            "WHERE industry IN (SELECT name " +
            "                   FROM salesforce.industries " +
            "                   WHERE name != :industry " +
            "                   ORDER BY embeddings <-> (SELECT embeddings FROM salesforce.industries WHERE name = :industry) LIMIT :limit)" +
            "ORDER BY name")
    Set<Account> findSimilaritiesForIndustry(String industry, int limit);
}
