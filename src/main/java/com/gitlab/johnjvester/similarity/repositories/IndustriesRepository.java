package com.gitlab.johnjvester.similarity.repositories;

import com.gitlab.johnjvester.similarity.entities.Industry;
import org.springframework.data.jpa.repository.JpaRepository;

public interface IndustriesRepository extends JpaRepository<Industry, String> { }
