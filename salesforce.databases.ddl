create table salesforce.account
(
    createddate    timestamp,
    isdeleted      boolean,
    name           varchar(255),
    systemmodstamp timestamp,
    accountnumber  varchar(40),
    industry       varchar(255),
    sfid           varchar(18),
    id             serial
        primary key,
    _hc_lastop     varchar(32),
    _hc_err        text
);

alter table salesforce.account
    owner to postgres;

create index hc_idx_account_systemmodstamp
    on salesforce.account (systemmodstamp);

create unique index hcu_idx_account_sfid
    on salesforce.account (sfid collate ucs_basic);


create table salesforce.industries
(
    name       varchar     not null
        constraint industries_pk
            primary key,
    embeddings vector(100) not null
);

alter table salesforce.industries
    owner to postgres;